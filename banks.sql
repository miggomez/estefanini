USE [master]
GO
/****** Object:  Database [banks]    Script Date: 16/11/2020 7:45:22 p. m. ******/
CREATE DATABASE [banks]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'banks', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.SQLEXPRESS\MSSQL\DATA\banks.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'banks_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.SQLEXPRESS\MSSQL\DATA\banks_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
 WITH CATALOG_COLLATION = DATABASE_DEFAULT
GO
ALTER DATABASE [banks] SET COMPATIBILITY_LEVEL = 150
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [banks].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [banks] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [banks] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [banks] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [banks] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [banks] SET ARITHABORT OFF 
GO
ALTER DATABASE [banks] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [banks] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [banks] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [banks] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [banks] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [banks] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [banks] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [banks] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [banks] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [banks] SET  DISABLE_BROKER 
GO
ALTER DATABASE [banks] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [banks] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [banks] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [banks] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [banks] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [banks] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [banks] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [banks] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [banks] SET  MULTI_USER 
GO
ALTER DATABASE [banks] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [banks] SET DB_CHAINING OFF 
GO
ALTER DATABASE [banks] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [banks] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [banks] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [banks] SET QUERY_STORE = OFF
GO
USE [banks]
GO
/****** Object:  Table [dbo].[banks]    Script Date: 16/11/2020 7:45:22 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[banks](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [varchar](80) NOT NULL,
	[account] [varchar](15) NOT NULL,
	[balance] [numeric](18, 0) NOT NULL,
	[gmf] [bit] NOT NULL,
	[idcliente] [int] NOT NULL,
 CONSTRAINT [PK_banks] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[clients]    Script Date: 16/11/2020 7:45:22 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[clients](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [varchar](80) NOT NULL,
	[identification] [varchar](12) NOT NULL,
	[user] [varchar](12) NOT NULL,
	[password] [varchar](12) NOT NULL,
 CONSTRAINT [PK_clients] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[transactions]    Script Date: 16/11/2020 7:45:22 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[transactions](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[value] [numeric](16, 0) NOT NULL,
	[value_mgf] [numeric](10, 0) NOT NULL,
	[account_target] [varchar](15) NULL,
	[isused] [bit] NULL,
	[avaliable_balance] [numeric](16, 0) NOT NULL,
	[type] [varchar](20) NOT NULL,
	[idbanco] [int] NOT NULL,
 CONSTRAINT [PK_transactions] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[banks]  WITH CHECK ADD  CONSTRAINT [FK_banks_clients] FOREIGN KEY([idcliente])
REFERENCES [dbo].[clients] ([id])
GO
ALTER TABLE [dbo].[banks] CHECK CONSTRAINT [FK_banks_clients]
GO
ALTER TABLE [dbo].[transactions]  WITH CHECK ADD  CONSTRAINT [FK_transactions_banks] FOREIGN KEY([idbanco])
REFERENCES [dbo].[banks] ([id])
GO
ALTER TABLE [dbo].[transactions] CHECK CONSTRAINT [FK_transactions_banks]
GO
USE [master]
GO
ALTER DATABASE [banks] SET  READ_WRITE 
GO
