﻿using services.Objects;
using services.Services;
using System;
using System.Collections.Generic;
using System.Text;
using System.Transactions;

namespace services.Interfaces
{
   public interface ITransaction
    {
        ObjResponse Save(ObjTransaction transaction);
        List<data.Transaction> GetWithDraws(string user);
        List<data.Transaction> GetTransactions(string user);
    }
}
