﻿using data;
using services.Services;
using System;
using System.Collections.Generic;
using System.Text;

namespace services.Interfaces
{
    public interface IBank
    {
        ObjResponse Save(ClientBank bank);   
    }
}
