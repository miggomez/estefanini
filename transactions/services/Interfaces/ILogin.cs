﻿using services.Objects;
using services.Services;
using System;
using System.Collections.Generic;
using System.Text;

namespace services.Interfaces
{
    public interface ILogin
    {
        ObjResponse Login(ObjUser user);
    }
}
