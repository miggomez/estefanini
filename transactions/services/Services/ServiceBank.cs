﻿using data;
using services.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace services.Services
{
    public class ServiceBank:IBank
    {

        private Context_transaction context = new Context_transaction();
        public ObjResponse Save(ClientBank clientbank) 
        {
            var _client = (from c in context.Clients where c.Identification == clientbank.Identification select c).FirstOrDefault();
            var bank = (from c in context.Banks where c.Account == clientbank.Account select c).FirstOrDefault();
            ObjResponse response = new ObjResponse();
            if (bank !=null) 
            {
                response.success = false;
                response.message = "Esa cuenta ya existe, favor verificar";
                return response;
            }
            if (_client== null)
            {
                response.success = false;
                response.message = "La cedula no esta registrada, favor verificar";
                return response;
            }
            else 
            {
                Bank _bank = new Bank
                {
                    Idcliente = _client.Id,
                    Name = clientbank.Name,
                    Account = clientbank.Account,
                    Balance = clientbank.Balance,
                    Gmf = clientbank.Gmf
                };
                var res = context.Banks.Add(_bank);
                context.SaveChanges();

                response.success = true;
                response.message = "Cuenta inscrita con exito";
                return response;
            }

        }
    }
}
