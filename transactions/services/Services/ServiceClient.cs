﻿using data;
using services.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace services.Services
{
    public class ServiceClient : IClient
    {
        private Context_transaction context = new Context_transaction();
        public ObjResponse Save(Client client)
        {
            context.Clients.Add(client);
            context.SaveChanges();
            ObjResponse response = new ObjResponse();
            response.success = true;
            response.message = "Cliente guardado";
            return response;

        }
    }
}
