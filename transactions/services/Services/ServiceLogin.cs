﻿using data;
using services.Interfaces;
using services.Objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace services.Services
{
    public class ServiceLogin : ILogin
    {
        Context_transaction context = new Context_transaction();
        public ObjResponse Login(ObjUser user)
        {

            var _user = (from c in context.Clients where c.User == user.User && c.Password == user.Password select c).FirstOrDefault();
            if (_user != null)
            {
                ObjResponse response = new ObjResponse();
                response.success = true;
                return response;
            }
            else
            {
                ObjResponse response = new ObjResponse();
                response.success = false;
                return response;
            }

        }
    }
}
