﻿using data;
using services.Interfaces;
using services.Objects;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace services.Services
{
    public class ServiceTransaction:ITransaction
    {
        Context_transaction context = new Context_transaction();
        decimal gmf = 0;
        public ObjResponse Save(ObjTransaction transaction)
        {
            ObjResponse response = new ObjResponse();
            decimal amount = 0;
            if (string.IsNullOrEmpty(transaction.AccountTarget))
            {
                var bank = (from b in context.Banks where b.Account == transaction.AccountOrigin select b).FirstOrDefault();
                
                if(bank==null) 
                {
                    response.success = false;
                    response.message = "La cuenta origen no existe favor validar";
                    return response;
                }
                //var gmf = (transaction.Value * 1000) / 4;
                if (bank.Gmf)
                {
                    gmf = (transaction.Value * 4) / 1000;
                }
                else 
                {
                    gmf = 0;
                }
                if(transaction.Value > 9600000) 
                {
                    gmf = (transaction.Value * 4) / 1000;
                }
                amount = transaction.Value + gmf;
                if (amount < bank.Balance)
                {

                    Transaction _transaction = new Transaction
                    {
                        AccountTarget = transaction.AccountTarget,
                        Value = transaction.Value,
                        AvaliableBalance = bank.Balance - amount,
                        Idbanco = bank.Id,
                        Type = "Retiro",
                        ValueMgf = gmf,
                        Isused = false

                    };
                    bank.Balance =bank.Balance - amount;
                    context.Transactions.Add(_transaction);
                    context.SaveChanges();
                    response.success = true;
                    response.message = "Transaccion exitosa";
                    return response;

                }
                else
                {
                    response.success = false;
                    response.message = "Fondos insuficientes";
                    return response;
                }
            }
            else 
            {
                var bank = (from b in context.Banks where b.Account == transaction.AccountOrigin select b).FirstOrDefault();
                var banktarget=(from b in context.Banks where b.Account == transaction.AccountTarget select b).FirstOrDefault();
                var withdraw = context.Transactions.Find(transaction.Id);
                //var amount = bank.Balance - transaction.Value;
                if (bank == null)
                {
                    response.success = false;
                    response.message = "La cuenta origen no existe favor validar";
                    return response;
                }
                if (bank == null)
                {
                    response.success = false;
                    response.message = "La cuenta origen no existe favor validar";
                    return response;
                }
                //var gmf = (transaction.Value * 1000) / 4;
                if (bank.Gmf)
                {
                    gmf = (withdraw.Value * 4) / 1000;
                }
                else
                {
                    gmf = 0;
                }
                amount = withdraw.Value + gmf;
                if (amount < bank.Balance)
                {

                    Transaction _transaction = new Transaction
                    {
                        AccountTarget = transaction.AccountTarget,
                        Value = withdraw.Value,
                        AvaliableBalance =bank.Balance - amount,
                        Idbanco = bank.Id,
                        Type = "Transferencia",
                        ValueMgf=gmf

                    };
                    
                    withdraw.Isused = true;
                    var tran = context.Transactions.Add(_transaction);
                    context.SaveChanges();
                    //bank.Balance = bank.Balance - amount;
                    //context.SaveChanges();
                    banktarget.Balance = banktarget.Balance + withdraw.Value;
                    context.SaveChanges();
                    response.success = true;
                    response.message = "Transaccion exitosa";
                    return response;

                }
                else
                {
                    response.success = false;
                    response.message = "Fondos insuficientes";
                    return response;
                }
            }

        }

        public List<data.Transaction> GetWithDraws(string user) 
        {
            var transactions = (from c in context.Clients
                                join b in context.Banks on
                                 c.Id equals b.Idcliente
                                join t in context.Transactions on c.Id equals t.Idbanco
                                where c.User == user && t.Type=="Retiro" && t.Isused==false
                                select t).Distinct().ToList();

            return transactions;
        }

        public List<data.Transaction> GetTransactions(string user) 
        {

            var transactions = (from c in context.Clients join b in context.Banks on 
                                c.Id equals b.Idcliente join t in context.Transactions on c.Id equals t.Idbanco where c.User==user select t).Distinct();

            return transactions.ToList();
        
        }

    }
}
