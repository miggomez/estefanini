﻿using System;
using System.Collections.Generic;
using System.Text;

namespace services.Objects
{
    public class ObjTransaction
    {
        public int Id { get; set; }
        public decimal Value { get; set; }
        public string AccountTarget { get; set; }
        public string AccountOrigin { get; set;}
        public decimal AvaliableBalance { get; set; }
        public string Type { get; set; }
    }
}
