﻿using System;
using System.Collections.Generic;
using System.Text;

namespace services
{
    public class ClientBank
    {
        public string Name { get; set; }
        public string Identification { get; set; }
        public string User { get; set; }
        public string Password { get; set; }
        public string Password1 { get; set; }
        public string Bank { get; set; }
        public string Account { get; set; }
        public decimal Balance { get; set; }
        public bool Gmf { get; set; }
        public int Idcliente { get; set; }
    }
}
