import { Component, OnInit } from '@angular/core';
import { LoginService } from '../services/login.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  user: string;
  password: string;
  response: any;

  constructor(private login: LoginService)
  {

  }

  ngOnInit()
  {

  }

  Autenticate()
  {
    let user = {
      User: this.user,
      Password: this.password
    }

    this.login.Autenticate(user).subscribe(res => {
      this.response = res;
      if (this.response.success)
      {
        //window.sessionStorage.setItem("user", this.user);
        window.location.href="/home"
      }
      else
      {
        alert('Usuario o contraseña invalida');
      }
    });

  }

}
