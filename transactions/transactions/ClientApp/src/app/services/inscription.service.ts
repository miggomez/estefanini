import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class InscriptionService {

  constructor(private http: HttpClient)
  {

  }

  Save(bank: any)
  {
    return this.http.post(this.GetUrl() + "/Bank/Save", bank);
  }

  GetUrl() {
    let protocol = window.location.protocol;
    let domain = window.location.host;
    let url = protocol + "//" + domain;
    console.log(url);
    return url;
  }

}
