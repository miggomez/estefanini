import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  constructor(private http: HttpClient)
  {

  }

  Autenticate(user: any)
  {
    return this.http.post(this.GetUrl() +"/Login/Autenticate",user);
  }

  GetUrl()
  {
    let protocol = window.location.protocol;
    let domain = window.location.host;
    let url = protocol + "//" + domain;
    console.log(url);
    return url;
  }

}
