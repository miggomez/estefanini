import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class TransactionsService {

  constructor(private http: HttpClient)
  {
    
  }

  Save(client: any)
  {
    return this.http.post(this.GetUrl() + "/Client/Save", client);
  }

  SaveTransaction(transaction: any)
  {
    return this.http.post(this.GetUrl() + "/Transaction/Save", transaction);
  }

  GetTransactions()
  {
    return this.http.get(this.GetUrl() +"/Transaction/GetTransactions");
  }

  GetWithDraws()
  {
    return this.http.get(this.GetUrl() +"/Transaction/GetWithDraws");
  }

  GetUrl()
  {
    let protocol = window.location.protocol;
    let domain = window.location.host;
    let url = protocol + "//" + domain;
    console.log(url);
    return url;
  }
}
