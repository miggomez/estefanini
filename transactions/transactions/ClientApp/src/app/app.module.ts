import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { RouterModule } from '@angular/router';

import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { TransactionsComponent } from './transactions/transactions.component';
import { IncriptionsComponent } from './incriptions/incriptions.component';
import { LoginComponent } from './login/login.component';
import { UserComponent } from './user/user.component';
import { MovementsComponent } from './movements/movements.component';


@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    TransactionsComponent,
    IncriptionsComponent,
    LoginComponent,
    UserComponent,
    MovementsComponent

  ],
  imports: [
    BrowserModule.withServerTransition({ appId: 'ng-cli-universal' }),
    HttpClientModule,
    FormsModule,
    RouterModule.forRoot([
      { path: '', component: LoginComponent, pathMatch: 'full' },
      { path: 'home', component: HomeComponent },
      { path: 'transactions', component: TransactionsComponent },
      { path: 'inscriptions', component: IncriptionsComponent },
      { path: 'register', component: UserComponent },
      { path: 'movements', component: MovementsComponent },
      
    ])
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
