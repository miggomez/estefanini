import { Component, OnInit } from '@angular/core';
import { TransactionsService } from '../services/transactions.service';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {

  name: string;
  identification: string;
  user: string;
  password: string;
  password1: string;
  response: any;

  constructor(private service: TransactionsService)
  {

  }

  ngOnInit() {
  }

  Save() {
    
    if (this.name.length < 3) {
      alert('El nombre esta demasiado corto');
      return;
    }

    if (this.identification.length < 6) {
      alert('Ese numero de cedula no es válido');
      return;
    }

    if (this.user.length < 5) {
      alert('El usuario es demasiado corto');
      return;
    }

    if (this.password.length < 8) {
      alert('La contraseña debe tener minimo 8 caracteres');
      return;
    }
    
    if (this.password != this.password1) {
      alert('Las contraseñas no conciden favor validar');
      return;
    }

    let client =
    {
      Name: this.name,
      Identification: this.identification,
      User: this.user,
      Password: this.password,
      Password1: this.password1
    }
    this.service.Save(client).subscribe(res => {
      debugger
      this.response = res;
      if (this.response.success) {
        alert(this.response.message);
        window.location.href = "/home";
      }
      else {
        alert(this.response.message);
      }

    });
  }

}
