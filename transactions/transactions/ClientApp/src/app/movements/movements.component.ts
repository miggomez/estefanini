import { Component, OnInit } from '@angular/core';
import { TransactionsService } from '../services/transactions.service';

@Component({
  selector: 'app-movements',
  templateUrl: './movements.component.html',
  styleUrls: ['./movements.component.css']
})
export class MovementsComponent implements OnInit {

  constructor(private service: TransactionsService)
  {
    this.GetTransactions();
  }
  data: any;

  ngOnInit()
  {

  }

  GetTransactions() {
    this.service.GetTransactions().subscribe(res => {
      this.data = res;
      console.log(res);
    });
  }

}
