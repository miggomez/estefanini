import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IncriptionsComponent } from './incriptions.component';

describe('IncriptionsComponent', () => {
  let component: IncriptionsComponent;
  let fixture: ComponentFixture<IncriptionsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IncriptionsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IncriptionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
