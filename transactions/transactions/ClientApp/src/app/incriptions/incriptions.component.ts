import { Component, OnInit } from '@angular/core';
import { InscriptionService } from '../services/inscription.service';

@Component({
  selector: 'app-incriptions',
  templateUrl: './incriptions.component.html',
  styleUrls: ['./incriptions.component.css']
})
export class IncriptionsComponent implements OnInit {


  bank: string;
  account: string;
  balance: string;
  gmf: string;
  response: any;
  identification: string;
  constructor(private service: InscriptionService)
  {

  }

  ngOnInit()
  {

  }

  Save()
  {

    if (this.gmf=="-1")
    {
      alert('Pofavor seleccione si excento o no');
      return;
    }

    if (this.bank == "0")
    {
      alert('Pofavor seleccione un banco');
      return;
    }

    if (this.account.length <= 0)
    {
      alert('El campo cuenta no debe estar vacio');
      return;
    }
    debugger
    let bankclient =
    {
      Name: this.bank,
      Account: this.account,
      Balance: parseInt(this.balance),
      Gmf: (this.gmf == "1" ? true : false),
      Identification: this.identification

    }
    this.service.Save(bankclient).subscribe(res => {
      this.response = res;
      if (this.response.success) {
        alert(this.response.message);
        this.account = "";
        this.balance = "";
        this.bank = "";
        this.gmf = "-1";
        this.identification = "";
      }
      else
      {
        alert(this.response.message);
      }
    });
  }

  SelectMgf(event: any)
  {
    this.gmf = event;
  }

  SelectBank(event: any)
  {
    this.bank = event;
  }

}
