import { Component, OnInit } from '@angular/core';
import { TransactionsService } from '../services/transactions.service';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
//import { read } from 'fs';

@Component({
  selector: 'app-transactions',
  templateUrl: './transactions.component.html',
  styleUrls: ['./transactions.component.css']
})
export class TransactionsComponent implements OnInit {


  name: string;
  identification: string;
  user: string;
  password: string;
  password1: string;
  response: any;
  value: number;
  accounttarget: string;
  accountorigin: string;
  available: string;
  showinput: boolean;
  trans: string;
  withdraws: any;
  aux: string;
  aux1: any;
  showcmb: boolean;
  showamounth: boolean;
  

  constructor(private service: TransactionsService)
  {

  }

  ngOnInit()
  {

  }

  Save()
  {
    debugger
    console.log(this.value);
    if (this.trans == undefined)
    {
      alert('Debe escojar un tipo de transaccion');
      return;
    }
    if (this.showinput) {
      if (this.accounttarget == "") {
        alert('La cuenta destino no puede ser nula');
        return;
      }

    } else if (this.accountorigin=="")
    {
      alert('Porfavor ingrese su numero de cuenta')
      return;
    }
    debugger
    let transaction = {
      Value: this.value,
      AccountTarget: this.accounttarget,
      AccountOrigin: this.accountorigin,
      Id: parseInt(this.aux)
    }
    this.service.SaveTransaction(transaction).subscribe(res => {
      this.response = res
      if (this.response.success) {
        this.accountorigin = "";
        this.accounttarget = "";
        this.value = null;
        this.GetWithDraws();
        alert(this.response.message);
      }
      else
      {
        alert(this.response.message);
      }
    });
  }

  SelectTransaction(event: any)
  {
    debugger
    if (event == "2")
    {
      this.showinput = true;
      this.showcmb = true;
      this.showamounth = false;
      this.trans = event;
      this.value = 0;
      this.GetWithDraws();
    }
    else
    {
      this.showinput = false;
      this.accounttarget = null;
      this.trans = event;
      this.showamounth = true;
      this.aux = "0";
    }
  }

  GetWithDraws()
  {
    this.service.GetWithDraws().subscribe(res => {
      this.withdraws = res;
    });
  }

  SelectWithDraw(e)
  {
    debugger;
    console.log(this.aux);
    //this.value = parseFloat(e);
  }

}
