﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using data;
using services.Interfaces;
using Microsoft.AspNetCore.Http;
using services.Objects;

namespace transactions.Controllers
{
    public class TransactionController : Controller
    {
        private ITransaction service;

        public TransactionController(ITransaction _service)
        {
            this.service = _service;
        }

        [HttpPost]
        public IActionResult Save([FromBody] ObjTransaction transaction)
        {
            try
            {
                var res = service.Save(transaction);
                return Ok(res);
            }
            catch (Exception e)
            {
                e.Message.ToString();
                return Ok("Error");
            }
        }

        [HttpGet]
        public IActionResult GetTransactions()
        {
            try
            {
                var res = service.GetTransactions(HttpContext.Session.GetString("user"));
                return Ok(res);
            }
            catch(Exception e) 
            {
                e.Message.ToString();
                return Ok("Error");
            }
        }

        [HttpGet]
        public IActionResult GetWithDraws(string user) 
        {
            try
            {
                var res = service.GetWithDraws(HttpContext.Session.GetString("user"));
                return Ok(res);
            }
            catch(Exception e) 
            {
                e.Message.ToString();
                return Ok("Error");
            }
        }
    }
}
