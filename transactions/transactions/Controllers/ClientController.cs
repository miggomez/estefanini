﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using data;
using Microsoft.AspNetCore.Mvc;
using services;
using services.Interfaces;

namespace transactions.Controllers
{
    public class ClientController : Controller
    {
        private IClient service;

        public ClientController(IClient _service) 
        {
            this.service = _service;
        }

        [HttpPost]
        public IActionResult Save([FromBody] Client client)
        {
            try
            {
                var res = service.Save(client);
                return Ok(res);
            }
            catch (Exception e)
            {
                e.Message.ToString();
                return Ok("Error");
            }
        }

    }
}
