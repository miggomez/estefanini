﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using services.Interfaces;
using services.Objects;
using services.Services;

namespace transactions.Controllers
{
   
    public class LoginController : Controller
    {
        private ILogin service;
        public LoginController(ILogin _service) 
        {
            this.service = _service;
        }

        [HttpPost]
        public IActionResult Autenticate([FromBody] ObjUser user)
        {

            try
            {
                var session = HttpContext.Session.GetString("user");
                ObjResponse response = new ObjResponse();
                if (session == null || session.Length == 0)
                {
                    var res = service.Login(user);
                    if (res.success)
                    {
                        HttpContext.Session.SetString("user", user.User);
                        response.success = true;
                        return Ok(res);
                    }
                    else
                    {
                        response.success = false;
                        return Ok(response);
                    }

                }
                else
                {
                    response.success = true;
                    return Ok(response);
                }
            }
            catch (Exception e)
            {
                e.Message.ToString();
                return Ok("Error");
            }
        }
    }
}
