﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

#nullable disable

namespace data
{
    public partial class Context_transaction : DbContext
    {
        public Context_transaction()
        {
        }

        public Context_transaction(DbContextOptions<Context_transaction> options)
            : base(options)
        {
        }

        public virtual DbSet<Bank> Banks { get; set; }
        public virtual DbSet<Client> Clients { get; set; }
        public virtual DbSet<Transaction> Transactions { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.
                optionsBuilder.UseSqlServer("data source=DESKTOP-OS82M88\\SQLEXPRESS;initial catalog=banks;user id=sa;password=1234");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Bank>(entity =>
            {
                entity.ToTable("banks");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Account)
                    .IsRequired()
                    .HasMaxLength(15)
                    .IsUnicode(false)
                    .HasColumnName("account");

                entity.Property(e => e.Balance)
                    .HasColumnType("numeric(18, 0)")
                    .HasColumnName("balance");

                entity.Property(e => e.Gmf).HasColumnName("gmf");

                entity.Property(e => e.Idcliente).HasColumnName("idcliente");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(80)
                    .IsUnicode(false)
                    .HasColumnName("name");

                entity.HasOne(d => d.IdclienteNavigation)
                    .WithMany(p => p.Banks)
                    .HasForeignKey(d => d.Idcliente)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_banks_clients");
            });

            modelBuilder.Entity<Client>(entity =>
            {
                entity.ToTable("clients");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Identification)
                    .IsRequired()
                    .HasMaxLength(12)
                    .IsUnicode(false)
                    .HasColumnName("identification");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(80)
                    .IsUnicode(false)
                    .HasColumnName("name");

                entity.Property(e => e.Password)
                    .IsRequired()
                    .HasMaxLength(12)
                    .IsUnicode(false)
                    .HasColumnName("password");

                entity.Property(e => e.User)
                    .IsRequired()
                    .HasMaxLength(12)
                    .IsUnicode(false)
                    .HasColumnName("user");
            });

            modelBuilder.Entity<Transaction>(entity =>
            {
                entity.ToTable("transactions");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.AccountTarget)
                    .HasMaxLength(15)
                    .IsUnicode(false)
                    .HasColumnName("account_target");

                entity.Property(e => e.AvaliableBalance)
                    .HasColumnType("numeric(16, 0)")
                    .HasColumnName("avaliable_balance");

                entity.Property(e => e.Idbanco).HasColumnName("idbanco");

                entity.Property(e => e.Isused).HasColumnName("isused");

                entity.Property(e => e.Type)
                    .IsRequired()
                    .HasMaxLength(20)
                    .IsUnicode(false)
                    .HasColumnName("type");

                entity.Property(e => e.Value)
                    .HasColumnType("numeric(16, 0)")
                    .HasColumnName("value");

                entity.Property(e => e.ValueMgf)
                    .HasColumnType("numeric(10, 0)")
                    .HasColumnName("value_mgf");

                entity.HasOne(d => d.IdbancoNavigation)
                    .WithMany(p => p.Transactions)
                    .HasForeignKey(d => d.Idbanco)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_transactions_banks");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
