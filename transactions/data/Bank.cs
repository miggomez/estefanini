﻿using System;
using System.Collections.Generic;

#nullable disable

namespace data
{
    public partial class Bank
    {
        public Bank()
        {
            Transactions = new HashSet<Transaction>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string Account { get; set; }
        public decimal Balance { get; set; }
        public bool Gmf { get; set; }
        public int Idcliente { get; set; }

        public virtual Client IdclienteNavigation { get; set; }
        public virtual ICollection<Transaction> Transactions { get; set; }
    }
}
