﻿using System;
using System.Collections.Generic;

#nullable disable

namespace data
{
    public partial class Client
    {
        public Client()
        {
            Banks = new HashSet<Bank>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string Identification { get; set; }
        public string User { get; set; }
        public string Password { get; set; }

        public virtual ICollection<Bank> Banks { get; set; }
    }
}
