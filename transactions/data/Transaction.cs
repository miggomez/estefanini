﻿using System;
using System.Collections.Generic;

#nullable disable

namespace data
{
    public partial class Transaction
    {
        public int Id { get; set; }
        public decimal Value { get; set; }
        public decimal ValueMgf { get; set; }
        public string AccountTarget { get; set; }
        public bool? Isused { get; set; }
        public decimal AvaliableBalance { get; set; }
        public string Type { get; set; }
        public int Idbanco { get; set; }

        public virtual Bank IdbancoNavigation { get; set; }
    }
}
